# Open Proton Bridge

Docker container that enables [proton-bridge](https://github.com/ProtonMail/proton-bridge) to act as a network service.

## What is the need?

ProtonMail is a great service, however, there are still some limitations when it comes to integration with other software, such as 3rd party email clients. For instance, the official solution [proton-bridge](https://github.com/ProtonMail/proton-bridge) cannot listen on interfaces other than localhost.

Using the power of TCP redirection through socat, we're able to expose those ports to our network, and point any 3rd party email clients to this service.

## Note on security

Since proton-bridge is not designed to be run as a network service please make sure you properly firewall and harden the server that you plan to run the container on. **Most importantly, make sure the TCP ports you specify in _SOCAT_IMAP_LISTEN_PORT_ and _SOCAT_SMTP_LISTEN_PORT_ are not port-forwarded to the internet**, and are only accessible from devices you intend to use email clients on.

## Usage

### Build the image

```bash
git clone https://gitlab.com/disastrpc/open-proton-bridge/
cd open-proton-bridge
docker build . --tag=open-proton-bridge
```

### Run the container
```bash
docker run \
    -d \
    --name open-proton-bridge \
    -p 1144:1144/tcp -p 1026:1026/tcp \
    --restart=unless-stopped \
    --hostname open-proton-bridge \
    open-proton-bridge
```

### Configure the service

First, open a shell inside the container.
```bash
docker exec -it open-proton-bridge bash
```
You will be dropped in the _/bridge_ folder in the root directory. Execute the bridge in CLI mode:

```bash
su proton
./bridge --cli
```

Once inside the proton-bridge prompt, execute the login command and add your desired account:

```bash
>>> login
Username: proton-username
Password:
Authenticating ...
Two factor code: 123456
Account proton-username was added successfully.
```

Bridge will then sync your emails to the local server, depending on the amount it could take some time. 

Once the sync is complete display the credentials for your account:
```bash
>>> list
# : account              (status         , address mode   )
0 : youraccount           (connected      , combined       )
>>> info 0
Configuration for proton-username@protonmail.com
IMAP Settings
Address:   127.0.0.1
IMAP port: 1143
Username:  proton-username@protonmail.com
Password:  <REDACTED>
Security:  STARTTLS

SMTP Settings
Address:   127.0.0.1
SMTP port: 1025
Username:  proton-username@protonmail.com
Password:  <REDACTED>
Security:  STARTTLS
```
You can now add this service to any 3rd party email clients by connecting to the opened ports in the container and using the credentials provided by the command above.

Start the systemd service inside the container:

```bash
systemctl start open-proton-bridge
```

On the host system, add this entry to your crontab to ensure the _open-proton-bridge_ service is restarted on reboot:

```bash
@reboot docker exec open-proton-bridge systemctl start open-proton-bridge
```

## Build Arguments

- **GO_ARCH (default: arm64)**: Set the architecture for the golang installation used to compile proton-bridge.
- **GO_VERSION (default: 1.21.3):** Set the Golang version to be installed.
- **S6_ARCH (default: aarch64):** Set the architecture for the S6 overlay installation.
- **S6_OVERLAY_VERSION (default: 3.1.5.0):** Set the version of S6 overlay to install.
- **GPG_IDENTITY_NAME (default: Proton Bridge):** Set the GPG name to be used when generating keys.
- **GPG_IDENTITY_EMAIL (default: proton@localhost):** Set the GPG name to be used when generating keys.
- **SOCAT_IMAP_LISTEN_PORT (default: 1144):** IMAP port the container will listen on.
- **SOCAT_SMTP_LISTEN_PORT (default: 1025):** SMTP port the container will listen on.

## To-do

- Migrate the open-proton-bridge service to execute as an s6-overlay longshot process instead of using systemd.
- Automate bridge setup

## Issues

I have tested this setup on a Raspberry Pi 4B 8GB. If you run into problems on different hardware/software, or have any suggestions, please submit an issue to this repo. Cheers!
