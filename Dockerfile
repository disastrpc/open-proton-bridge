FROM debian:stable-slim AS build

ARG GO_ARCH=arm64
ARG GO_VERSION=1.22.0

# Update and install packages
RUN echo "deb http://deb.debian.org/debian bullseye-backports main" >> /etc/apt/sources.list
RUN apt-get update && apt-get install -y --no-install-recommends \
	git \
	make \
	ca-certificates \
	build-essential \
	pkg-config \
	dbus-x11 \
	libsecret-common \
	libsecret-1-dev \
	libglvnd-core-dev \
	libglvnd-dev && \
	rm -rf /var/lib/apt/lists/*

# Setup golang install
ADD https://go.dev/dl/go${GO_VERSION}.linux-${GO_ARCH}.tar.gz /tmp/
ENV PATH="${PATH}:/usr/lib/go/bin"
RUN tar -C /usr/lib -xzf /tmp/go${GO_VERSION}.linux-${GO_ARCH}.tar.gz

# Compile proton bridge
RUN git clone https://github.com/ProtonMail/proton-bridge /tmp/proton-bridge
WORKDIR /tmp/proton-bridge
RUN make build-nogui
WORKDIR /bridge
RUN cp /tmp/proton-bridge/proton-bridge . && \
	cp /tmp/proton-bridge/bridge .

FROM debian:stable-slim as s6
USER root

ARG S6_ARCH=aarch64
ARG S6_OVERLAY_VERSION=3.1.6.2
ARG GPG_IDENTITY_NAME=Proton Bridge
ARG GPG_IDENTITY_EMAIL=proton@localhost
ARG SOCAT_SMTP_LISTEN_PORT=1026
ARG SOCAT_IMAP_LISTEN_PORT=1144


ENV GPG_IDENTITY_NAME $GPG_IDENTITY_NAME
ENV GPG_IDENTITY_EMAIL $GPG_IDENTITY_EMAIL
ENV SOCAT_IMAP_LISTEN_PORT $SOCAT_IMAP_LISTEN_PORT
ENV SOCAT_SMTP_LISTEN_PORT $SOCAT_SMTP_LISTEN_PORT

WORKDIR /bridge
COPY --from=build ./bridge .
RUN apt-get update && apt-get install -y --no-install-recommends \
	pass \
	socat \
	systemctl \
	dbus-x11 \
	xz-utils \
	ca-certificates \
	libsecret-common \
	libsecret-1-dev \
	libglvnd-core-dev \
	libglvnd-dev

# Configure GPG keys for email relay
RUN useradd -m -d /home/proton -s /bin/bash proton && \
	chown -R proton:proton /bridge && \
	chown -R proton:proton /home/proton

USER proton
SHELL ["/bin/bash", "-c"]
RUN gpg --list-keys && \
	echo $'use-agent\npinentry-mode loopback\n' > ~/.gnupg/gpg.conf && \
	echo 'allow-loopback-pinentry' > ~/.gnupg/gng-agent.conf && \
	echo RELOADAGENT | gpg-connect-agent && \
	echo -e $" \n\
	Key-Type: 1 \n\
	Key-Length: 4096 \n\
	Subkey-Type: 1 \n\
	Subkey-Length: 4096 \n\
	Name-Real: $GPG_IDENTITY_NAME \n\
	Name-Email: $GPG_IDENTITY_EMAIL \n\
	Expire-Date: 0 \n\
	%no-protection" | gpg --batch --full-gen-key --no-tty - && \
	gpg --fingerprint | \
	sed -n '/^\s/s/\s*//p' | \
	sed -e "s/ //g" | \
	{ id=$(cat /dev/stdin); pass init $id; }

USER root
# Create socat relays and add them to s6-overlay startup
RUN mkdir -p /etc/s6-overlay/s6-rc.d/user/contents.d/
RUN echo '#!/bin/bash' > /usr/local/bin/start_relay_smtp.sh && \
	echo "socat TCP-LISTEN:$SOCAT_SMTP_LISTEN_PORT,reuseaddr,fork TCP-CONNECT:localhost:1025" >> /usr/local/bin/start_relay_smtp.sh && \
	chmod +x /usr/local/bin/start_relay_smtp.sh && \
	mkdir -p /etc/s6-overlay/s6-rc.d/socat_smtp/ && \
	echo 'oneshot' > /etc/s6-overlay/s6-rc.d/socat_smtp/type && \
	echo '/usr/local/bin/start_relay_smtp.sh' > /etc/s6-overlay/s6-rc.d/socat_smtp/up && \
	touch /etc/s6-overlay/s6-rc.d/user/contents.d/socat_smtp

RUN echo '#!/bin/bash' > /usr/local/bin/start_relay_imap.sh && \
	echo "socat TCP-LISTEN:$SOCAT_IMAP_LISTEN_PORT,reuseaddr,fork TCP-CONNECT:localhost:1143" >> /usr/local/bin/start_relay_imap.sh && \
	chmod +x /usr/local/bin/start_relay_imap.sh && \
	mkdir -p /etc/s6-overlay/s6-rc.d/socat_imap/ && \
	echo 'oneshot' > /etc/s6-overlay/s6-rc.d/socat_imap/type && \
	echo '/usr/local/bin/start_relay_imap.sh' > /etc/s6-overlay/s6-rc.d/socat_imap/up && \
	touch /etc/s6-overlay/s6-rc.d/user/contents.d/socat_imap

RUN echo $'[Unit] \n\
Description=Open ProtonBridge \n\
Type=simple \n\
Restart=always \n\
RestartSec=1 \n\
\n\
[Service] \n\
User=proton \n\
ExecStart=/bridge/bridge --noninteractive \n\
\n\
[Install] \n\
WantedBy=multi-user.target' > /etc/systemd/system/open-proton-bridge.service

# Setup s6-overlay
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-${S6_ARCH}.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-${S6_ARCH}.tar.xz
ENTRYPOINT ["/init"]
